<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Session;

class PrepareSessionsData
{
    public function execute(array $sessions): array
    {
        $sessionsData = [];

        foreach ($sessions as $item) {
            $productModel = $item->getProductModel();
            $productData = $productModel->getData();
            $productData['model'] = $productModel;
            $sessionData = $item->getData();
            unset($sessionData['product_model']);
            $sessionExtraData = [
                'product' => $productData,
                'location_name' => $item->getLocationName(),
                'date_time' => $item->getDateTime()->getShortValue(),
                'instructor_name' => $item->getInstructorName()
            ];
            $sessionsData[] = array_merge($sessionData, $sessionExtraData);
        }

        return $sessionsData;
    }
}
