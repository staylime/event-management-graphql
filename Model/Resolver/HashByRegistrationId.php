<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class HashByRegistrationId implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\RegistrationRepository
     */
    private $registrationRepository;

    public function __construct(
        \Staylime\ClassManager\Model\RegistrationRepository $registrationRepository
    ) {
        $this->registrationRepository = $registrationRepository;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): string {
        if (empty($args['registrationId'])) {
            throw new GraphQlInputException(__('Required parameter "registrationId" is missing'));
        }

        try {
            $registration = $this->registrationRepository->getById($args['registrationId']);
        } catch (\Throwable $e) {
            throw new GraphQlNoSuchEntityException(
                __('Could not find a registration with ID "%id"', ['id' => $args['sessionId']])
            );
        }

        return $registration->getHash();
    }
}
