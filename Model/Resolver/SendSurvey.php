<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Staylime\ClassManager\Model\Registration;

class SendSurvey implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Api\RegistrationRepositoryInterface
     */
    private $registrationRepository;

    /**
     * @var \Staylime\ClassManager\Api\SurveyResultRepositoryInterface
     */
    private $surveyResultRepository;

    /**
     * @var \Staylime\ClassManager\Model\SurveyRepository
     */
    private $surveyRepository;

    /**
     * @var \Staylime\ClassManager\Api\Data\SurveyResultInterfaceFactory
     */
    private $surveyResultFactory;

    public function __construct(
        \Staylime\ClassManager\Api\RegistrationRepositoryInterface $registrationRepository,
        \Staylime\ClassManager\Api\SurveyResultRepositoryInterface $surveyResultRepository,
        \Staylime\ClassManager\Model\SurveyRepository $surveyRepository,
        \Staylime\ClassManager\Api\Data\SurveyResultInterfaceFactory $surveyResultFactory
    ) {
        $this->registrationRepository = $registrationRepository;
        $this->surveyResultRepository = $surveyResultRepository;
        $this->surveyRepository = $surveyRepository;
        $this->surveyResultFactory = $surveyResultFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): \Magento\Framework\Phrase {
        if (empty($args['input']['registration_id'])) {
            throw new GraphQlInputException(__('Required parameter "registration_id" is missing'));
        }

        if (empty($args['input']['hash'])) {
            throw new GraphQlInputException(__('Required parameter "hash" is missing'));
        }

        if (empty($args['input']['options'])) {
            throw new GraphQlInputException(__('Required parameter "options" is missing'));
        }

        $registration = $this->initRegistration(
            $args['input']['hash'],
            $args['input']['registration_id']
        );

        $survey = $registration->getSurvey() ?: $this->surveyRepository->getActive();

        if (!$survey) {
            return __('Default survey not found');
        }

        $options = $this->resolveOptions($args['input']['options']);
        unset($args['input']['options']);
        $post = array_merge($args['input'], $options);
        $result = $this->surveyResultFactory->create();

        $result
            ->applySurveyPost($survey, $post)
            ->setRegistrationModel($registration);

        try {
            $this->surveyResultRepository->save($result);

            return __('Thank you! Your feedback has been received.');
        } catch (\Magento\Framework\Exception\CouldNotSaveException $e) {
            return __('The survey for this class has already been completed.');
        } catch (\Exception $e) {
            return __($e->getMessage());
        }
    }

    private function resolveOptions(array $options): array
    {
        $result = [];

        foreach ($options as $option) {
            $result['option_' . $option['option_id']] = $option['option_data'];
        }

        return $result;
    }

    private function initRegistration(string $hash, string $registration_id): Registration
    {
        $registration = $this->registrationRepository->getById($registration_id);

        if (!$registration->checkHash($hash)) {
            throw new GraphQlInputException(__('Hash doesn\'t fit'));
        }

        return $registration;
    }
}
