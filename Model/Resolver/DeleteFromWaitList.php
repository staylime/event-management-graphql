<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Staylime\ClassManager\Api\Data\RegistrationInterface;

class DeleteFromWaitList implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Api\RegistrationRepositoryInterfaceFactory
     */
    private $registrationRepositoryInterfaceFactory;

    /**
     * @var \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Staylime\ClassManager\Api\RegistrationRepositoryInterfaceFactory $registrationRepositoryInterfaceFactory,
        \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory $collectionFactory
    ) {
        $this->registrationRepositoryInterfaceFactory = $registrationRepositoryInterfaceFactory;
        $this->collectionFactory = $collectionFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): \Magento\Framework\Phrase {
        $currentUserId = (int)$context->getUserId();

        if (empty($args['input']['session_id'])) {
            throw new GraphQlInputException(__('Required parameter "session_id" is missing'));
        }

        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The request is allowed for logged in customer'));
        } else {
            try {
                /** @var \Staylime\ClassManager\Model\ResourceModel\Registration\Collection $_collection */
                $collection = $this->collectionFactory->create();
                $collection
                    ->addFilter(RegistrationInterface::FIELD_CUSTOMER_ID, $currentUserId)
                    ->addFilter(RegistrationInterface::FIELD_SESSION_ID, (int)$args['input']['session_id'])
                    ->addFilter(RegistrationInterface::FIELD_STATUS, RegistrationInterface::STATUS_WAIT_LIST)
                    ->load();

                if (!$collection->count()) {
                    return __("Your the wait list spot isn't found");
                } else {
                    /** @var \Staylime\ClassManager\Model\Registration $registration */
                    $registration = $collection->getFirstItem();
                    /** @var \Staylime\ClassManager\Model\RegistrationRepository $registrationRepository */
                    $registrationRepository = $this->registrationRepositoryInterfaceFactory->create();
                    $registrationRepository->delete($registration);
                }
            } catch (\Exception $e) {
                throw new GraphQlInputException(__($e->getMessage()));
            }
        }

        return __("You deleted the wait list spot for this class.");
    }
}
