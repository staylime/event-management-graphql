<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Staylime\ClassManager\Helper\Config as Config;

class Survey implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\SessionRepository
     */
    private $sessionRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var \Staylime\ClassManager\Api\SurveyRepositoryInterfaceFactory
     */
    private $surveyRepositoryFactory;

    public function __construct(
        \Staylime\ClassManager\Model\SessionRepository $sessionRepository,
        Config $config,
        \Staylime\ClassManager\Api\SurveyRepositoryInterfaceFactory $surveyRepositoryFactory
    ) {
        $this->sessionRepository = $sessionRepository;
        $this->config = $config;
        $this->surveyRepositoryFactory = $surveyRepositoryFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        if (empty($args['sessionId'])) {
            throw new GraphQlInputException(__('Required parameter "sessionId" is missing'));
        }

        try {
            $session = $this->sessionRepository->getById($args['sessionId']);
            $survey = $session->getSurvey();
        } catch (\Throwable $e) {
            throw new GraphQlNoSuchEntityException(
                __('Could not find a session with ID "%id"', ['id' => $args['sessionId']])
            );
        }

        $defaultSurveyId = $this->config->getValue(Config::CONFIG_PATH_SURVEY_ENTITY) ?: 0;

        if ($survey === null) {
            try {
                $survey = $this->surveyRepositoryFactory->create()->getById($defaultSurveyId);
            } catch (\Throwable $e) {
                throw new GraphQlNoSuchEntityException(
                    __('Could not find a survey with ID "%id"', ['id' => $defaultSurveyId])
                );
            }
        }

        return $survey->getData();
    }
}
