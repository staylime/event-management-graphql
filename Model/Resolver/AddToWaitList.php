<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Staylime\ClassManager\Api\Data\RegistrationInterface;

class AddToWaitList implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Api\SessionRepositoryInterfaceFactory
     */
    private $sessionRepositoryInterfaceFactory;

    /**
     * @var \Staylime\ClassManager\Api\RegistrationRepositoryInterfaceFactory
     */
    private $registrationRepositoryInterfaceFactory;

    /**
     * @var \Staylime\ClassManager\Model\RegistrationFactory
     */
    private $registrationFactory;

    /**
     * @var \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    private $customerRegistry;

    public function __construct(
        \Staylime\ClassManager\Api\SessionRepositoryInterfaceFactory $sessionRepositoryInterfaceFactory,
        \Staylime\ClassManager\Api\RegistrationRepositoryInterfaceFactory $registrationRepositoryInterfaceFactory,
        \Staylime\ClassManager\Model\RegistrationFactory $registrationFactory,
        \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry
    ) {
        $this->sessionRepositoryInterfaceFactory = $sessionRepositoryInterfaceFactory;
        $this->registrationRepositoryInterfaceFactory = $registrationRepositoryInterfaceFactory;
        $this->registrationFactory = $registrationFactory;
        $this->collectionFactory = $collectionFactory;
        $this->customerRegistry = $customerRegistry;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): \Magento\Framework\Phrase {
        $currentUserId = (int)$context->getUserId();

        if (empty($args['input']['session_id'])) {
            throw new GraphQlInputException(__('Required parameter "session_id" is missing'));
        }

        if (empty($args['input']['quantity'])) {
            throw new GraphQlInputException(__('Required parameter "quantity" is missing'));
        }

        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The request is allowed for logged in customer'));
        } else {
            try {
                /** @var \Staylime\ClassManager\Model\RegistrationRepository $registrationRepository */
                $registrationRepository = $this->registrationRepositoryInterfaceFactory->create();
                /** @var \Staylime\ClassManager\Model\SessionRepository $sessionRepository */
                $sessionRepository = $this->sessionRepositoryInterfaceFactory->create();
                $customer = $this->customerRegistry->retrieve($currentUserId);
                $session = $sessionRepository->getById((int)$args['input']['session_id']);

                if (!$session->isCanWaitListReserve()) {
                    throw new GraphQlInputException(__('This session can\'t be added to the wait list'));
                } else {
                    $registration = null;

                    /** @var \Staylime\ClassManager\Model\ResourceModel\Registration\Collection $collection */
                    $collection = $this->collectionFactory->create();
                    $collection
                        ->addFilter(RegistrationInterface::FIELD_CUSTOMER_ID, $currentUserId)
                        ->addFilter(RegistrationInterface::FIELD_SESSION_ID, $session->getId())
                        ->addFilter(RegistrationInterface::FIELD_STATUS, RegistrationInterface::STATUS_WAIT_LIST)
                        ->load();

                    if ($collection->count()) {
                        $registration = $collection->getFirstItem();
                    }


                    if (!$registration) {
                        /** @var \Staylime\ClassManager\Model\Registration $registration */
                        $registration = $this->registrationFactory->create();

                        $registration
                            ->setSessionModel($session)
                            ->setCustomerModel($customer)
                            ->setIsWaitList()
                            ->setReservedDate(date('Y-m-d H:i:s'));
                    }

                    $quantity = abs((integer)$args['input']['quantity']);

                    if ($quantity > 100) {
                        $quantity = 100;
                    } elseif ($quantity < 1) {
                        $quantity = 1;
                    }

                    $registration
                        ->setPhone($customer->getDefaultBillingAddress()->getTelephone())
                        ->setInfo($args['input']['notes'] ?? '')
                        ->setQuantity($quantity);
                    $registrationRepository->save($registration);
                }
            } catch (\Exception $e) {
                throw new GraphQlInputException(__($e->getMessage()));
            }
        }

        return __("Good news! You're on the wait list for this class.");
    }
}
