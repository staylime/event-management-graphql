<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Staylime\ClassManager\Api\Data\LocationInterface;

class Locations implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\ResourceModel\Location\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Staylime\ClassManager\Model\ResourceModel\Location\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $instructorsData = [];
        $collection = $this->collectionFactory->create();

        if (!empty($args['is_filterable']) && $args['is_filterable'] === 1) {
            $collection->addFilter(LocationInterface::FIELD_IS_FILTERABLE, 1);
        }

        foreach ($collection as $item) {
            $instructorsData[] = $item->getData();
        }

        return $instructorsData;
    }
}
