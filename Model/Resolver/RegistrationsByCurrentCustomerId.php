<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Staylime\ClassManager\Api\Data\RegistrationInterface;
use Staylime\ClassManager\Block\Customer\Info;
use Staylime\ClassManager\Model\ResourceModel\Registration\Collection;

class RegistrationsByCurrentCustomerId implements ResolverInterface
{
    public const TYPE_UPCOMING = 1;

    public const TYPE_WAIT_LIST = 2;

    public const TYPE_RECENT_CLASSES = 3;

    /**
     * @var \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $timezoneInterface;

    /**
     * @var Info
     */
    private $blockCustomerInfo;

    public function __construct(
        \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory $collectionFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface,
        Info $blockCustomerInfo
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->timezoneInterface = $timezoneInterface;
        $this->blockCustomerInfo = $blockCustomerInfo;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $currentUserId = (int)$context->getUserId();

        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The request is allowed for logged in customer'));
        }

        if (empty($args['typeId'])) {
            throw new GraphQlInputException(__('Required parameter "typeId" is missing'));
        }

        if ($currentUserId) {
            $data = $this->prepareData($currentUserId, $args['typeId']);
        } else {
            throw new GraphQlNoSuchEntityException(
                __('Customer is not authorized.')
            );
        }

        return $data;
    }

    private function prepareData(int $currentUserId, int $typeId): array
    {
        $registrationsData = [];
        $collection = $this->getCollection($currentUserId, $typeId);

        foreach ($collection as $item) {
            $sessionModel = $item->getSessionModel();
            $productModel = $sessionModel->getProductModel();
            $productData = $productModel->getData();
            $productData['model'] = $productModel;
            $sessionData = $sessionModel->getData();
            unset($sessionData['product_model']);
            $sessionExtraData = [
                'product' => $productData,
                'location_name' => $sessionModel->getLocationName(),
                'date_time' => $sessionModel->getDateTime()->getShortValue(),
                'instructor_name' => $sessionModel->getInstructorName()
            ];
            $sessionData = array_merge($sessionData, $sessionExtraData);
            $registrationData = $item->getData();
            $registrationExtraData = [
                'has_reserved' => $this->hasReserved($currentUserId),
                'status_code' => $this->blockCustomerInfo->getStatusCode($item),
                'zoom_meeting_link' => $this->blockCustomerInfo->getZoomMeetingLink($sessionModel, $item),
                'status_name' => $this->blockCustomerInfo->getStatusName($item),
                'calendar_file_link' => $this->blockCustomerInfo->getCalendarFileLink($sessionModel->getId()),
                'hours_before_show_link' => $this->blockCustomerInfo->getHoursBeforeShowLink(),
                'session' => $sessionData
            ];
            $registrationsData[] = array_merge($registrationData, $registrationExtraData);
        }

        return $registrationsData;
    }

    private function hasReserved(int $currentUserId): bool
    {
        $collection = $this->collectionFactory
            ->createWithEmail($currentUserId)
            ->addFieldToSelect(RegistrationInterface::JOINED_FIELD_ORDER_INCREMENT_ID)
            ->addFieldToFilter(RegistrationInterface::FIELD_STATUS, ['eq' => RegistrationInterface::STATUS_RESERVED])
            ->setPageSize(1);

        return $collection->count() > 0;
    }

    private function getCollection(int $currentUserId, int $typeId): Collection
    {
        $now = $this->timezoneInterface->date()->format('Y-m-d H:i:s');
        $halfYearLimit = date('Y-m-d H:i:s', strtotime('-6 month'));
        $collection = $this->collectionFactory->createWithEmail($currentUserId);
        $collection->setFilterFutureSessions($halfYearLimit);

        if ($typeId === self::TYPE_WAIT_LIST) {
            $collection
                ->addFieldToFilter(RegistrationInterface::FIELD_STATUS, RegistrationInterface::STATUS_WAIT_LIST)
                ->setFilterFutureSessions($now);
            $collection->setOrderBySessionDate();
        } else {
            $collection
                ->addFieldToSelect(RegistrationInterface::JOINED_FIELD_ORDER_INCREMENT_ID)
                ->addFieldToFilter(RegistrationInterface::FIELD_STATUS, ['nin' => [
                    RegistrationInterface::STATUS_RESERVED, RegistrationInterface::STATUS_WAIT_LIST
                ]]);

            if ($typeId === 3) {
                $collection->setFilterPastSessions($now);
                $collection->setOrderBySessionDate('DESC');
            } else {
                $collection->setFilterFutureSessions($now);
                $collection->setOrderBySessionDate();
            }
        }

        return $collection;
    }
}
