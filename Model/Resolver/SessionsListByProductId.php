<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class SessionsListByProductId implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\SessionRepository
     */
    private $sessionRepository;

    /**
     * @var \Staylime\ClassManagerGraphQl\Model\Session\PrepareSessionsData
     */
    private $prepareSessionsData;

    public function __construct(
        \Staylime\ClassManager\Model\SessionRepository $sessionRepository,
        \Staylime\ClassManagerGraphQl\Model\Session\PrepareSessionsData $prepareSessionsData
    ) {
        $this->sessionRepository = $sessionRepository;
        $this->prepareSessionsData = $prepareSessionsData;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        if (empty($args['productId'])) {
            throw new GraphQlInputException(__('Required parameter "productId" is missing'));
        }

        if (!empty($args['statuses']) && !is_array($args['statuses'])) {
            throw new GraphQlInputException(__('Parameter "statuses" must be array'));
        }

        $items = $this->sessionRepository->getListByProductId(
            $args['productId'],
            true,
            $args['statuses'] ?? []
        )->getItems();

        return $this->prepareSessionsData->execute($items);
    }
}
