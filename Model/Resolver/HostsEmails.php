<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class HostsEmails implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\Zoom
     */
    private $zoomModel;

    public function __construct(
        \Staylime\ClassManager\Model\Zoom $zoomModel
    ) {
        $this->zoomModel = $zoomModel;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): ?array {
        if (empty($args['dateStart'])) {
            throw new GraphQlInputException(__('Required parameter "dateStart" is missing'));
        }

        if (empty($args['timeStart'])) {
            throw new GraphQlInputException(__('Required parameter "timeStart" is missing'));
        }

        if (empty($args['dateEnd'])) {
            throw new GraphQlInputException(__('Required parameter "dateEnd" is missing'));
        }

        if (empty($args['timeEnd'])) {
            throw new GraphQlInputException(__('Required parameter "timeEnd" is missing'));
        }

        $dateRange = json_encode(
            [
                'date_start' => $args['dateStart'],
                'time_start' => $args['timeStart'],
                'date_end' => $args['dateEnd'],
                'time_end' => $args['timeEnd']
            ]
        );

        return json_decode($this->zoomModel->getAvailableHostsListByDateRange($dateRange));
    }
}
