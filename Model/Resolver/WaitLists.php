<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class WaitLists implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\CustomerRegistrationRepository
     */
    private $customerRegistrationRepository;

    public function __construct(
        \Staylime\ClassManager\Model\CustomerRegistrationRepository $customerRegistrationRepository
    ) {
        $this->customerRegistrationRepository = $customerRegistrationRepository;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $currentUserId = (int)$context->getUserId();

        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The request is allowed for logged in customer'));
        }

        if (empty($args['sessionId'])) {
            throw new GraphQlInputException(__('Required parameter "sessionId" is missing'));
        }

        $waitListsData = [];

        if ($currentUserId) {
            $waitLists = $this->customerRegistrationRepository
                ->getWaitListBySessionId($args['sessionId'], $currentUserId)
                ->getItems();

            foreach ($waitLists as $item) {
                $waitListsData[] = $item->getData();
            }
        } else {
            throw new GraphQlNoSuchEntityException(
                __('Customer is not authorized.')
            );
        }

        return $waitListsData;
    }
}
