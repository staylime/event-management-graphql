<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Instructors implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\ResourceModel\Instructor\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Staylime\ClassManager\Model\ResourceModel\Instructor\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $instructorsData = [];
        $collection = $this->collectionFactory->create();

        foreach ($collection as $item) {
            $instructorsData[] = $item->getData();
        }

        return $instructorsData;
    }
}
