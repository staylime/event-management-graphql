<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Sessions implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\ResourceModel\Session\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Staylime\ClassManagerGraphQl\Model\Session\PrepareSessionsData
     */
    private $prepareSessionsData;

    public function __construct(
        \Staylime\ClassManager\Model\ResourceModel\Session\CollectionFactory $collectionFactory,
        \Staylime\ClassManagerGraphQl\Model\Session\PrepareSessionsData $prepareSessionsData
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->prepareSessionsData = $prepareSessionsData;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $collection = $this->collectionFactory->create();

        return $this->prepareSessionsData->execute($collection->getItems());
    }
}
