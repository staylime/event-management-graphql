<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class CustomSessionsListByLocationAndDate implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\CustomSessionRepository
     */
    private $customSessionRepository;

    public function __construct(
        \Staylime\ClassManager\Model\CustomSessionRepository $customSessionRepository
    ) {
        $this->customSessionRepository = $customSessionRepository;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        if (empty($args['locationId'])) {
            throw new GraphQlInputException(__('Required parameter "quoteId" is missing'));
        }

        if (empty($args['dateFrom'])) {
            throw new GraphQlInputException(__('Required parameter "quoteItems" is missing'));
        }

        if (empty($args['dateTo'])) {
            throw new GraphQlInputException(__('Required parameter "quoteItems" is missing'));
        }

        $sessionsData = [];
        $items = $this->customSessionRepository->getListByLocationAndDate(
            $args['locationId'],
            [$args['dateFrom'], $args['dateTo']]
        )->getItems();

        foreach ($items as $item) {
            $sessionData = $item->getData();
            unset($sessionData['product_model']);
            $sessionExtraData = [
                'location_name' => $item->getLocationName(),
                'date_time' => $item->getDateTime()->getShortValue(),
                'instructor_name' => $item->getInstructorName()
            ];
            $sessionsData[] = array_merge($sessionData, $sessionExtraData);
        }

        return $sessionsData;
    }
}
