<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Registrations implements ResolverInterface
{
    /**
     * @var \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Staylime\ClassManager\Model\ResourceModel\Registration\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): array {
        $registrationsData = [];
        $collection = $this->collectionFactory->create();

        foreach ($collection as $item) {
            $sessionModel = $item->getSessionModel();
            $productModel = $sessionModel->getProductModel();
            $productData = $productModel->getData();
            $productData['model'] = $productModel;
            $sessionData = $sessionModel->getData();
            unset($sessionData['product_model']);
            $sessionExtraData = [
                'product' => $productData,
                'location_name' => $sessionModel->getLocationName(),
                'date_time' => $sessionModel->getDateTime()->getShortValue(),
                'instructor_name' => $sessionModel->getInstructorName()
            ];
            $sessionData = array_merge($sessionData, $sessionExtraData);
            $registrationData = $item->getData();
            $registrationExtraData = [
                'session' => $sessionData
            ];
            $registrationsData[] = array_merge($registrationData, $registrationExtraData);
        }

        return $registrationsData;
    }
}
