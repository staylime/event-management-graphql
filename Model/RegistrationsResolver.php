<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model;

use Magento\Framework\GraphQl\Query\Resolver\TypeResolverInterface;

class RegistrationsResolver implements TypeResolverInterface
{
    public function resolveType(array $data): string
    {
        return 'Registrations';
    }
}
