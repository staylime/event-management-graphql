<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Model;

use Magento\Framework\GraphQl\Query\Resolver\TypeResolverInterface;

class SessionsResolver implements TypeResolverInterface
{
    public function resolveType(array $data): string
    {
        return 'Sessions';
    }
}
