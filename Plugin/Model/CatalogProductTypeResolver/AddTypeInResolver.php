<?php

declare(strict_types=1);

namespace Staylime\ClassManagerGraphQl\Plugin\Model\CatalogProductTypeResolver;

use Magento\CatalogGraphQl\Model\CatalogProductTypeResolver;

class AddTypeInResolver
{
    public function afterResolveType(CatalogProductTypeResolver $subject, string $result, array $data): string
    {
        if (isset($data['type_id'])) {
            if ($data['type_id'] === 'tl_class_reservation') {
                return 'SimpleProduct';
            }
        }

        return $result;
    }
}
