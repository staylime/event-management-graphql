# Staylime_ClassManagerGraphQl

GraphQL API module for Staylime [Event Management](https://staylime.com/products/event-management/) extension.

## Installation
**1) Copy-to-paste method**
- Download this module and upload it to the `app/code/Staylime/ClassManagerGraphQl` directory

**2) Installation using composer (from packagist)**
- Execute the following command: `composer require staylime/classmanager-graphql`

## How to use
Coming soon
