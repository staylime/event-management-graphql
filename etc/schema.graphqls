type Query {
    registrationsByCurrentCustomerId (
        typeId: Int! @doc(description: "Can be of three types: 1 - upcoming, 2 - wish list, 3 - recent classes")
    ): [RegistrationWithDataForCustomer] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\RegistrationsByCurrentCustomerId") @doc(description:"Returns a registration entities with all necessary information by current customer id.") @cache(cacheable: false),
    classManagerRegistrations: [Registration] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\Registrations") @doc(description:"Returns a registration entities.") @cache(cacheable: false),
    classManagerSessions: [ClassSession] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\Sessions") @doc(description:"Returns a session entities.") @cache(cacheable: false),
    classManagerInstructors: [Instructor] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\Instructors") @doc(description:"Returns a instructor entities.") @cache(cacheable: false),
    classManagerLocations (
        is_filterable: Int @doc(description: "Is Locations Filterable. Can be 1 or 0, default 0.")
    ): [Location] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\Locations") @doc(description:"Returns a location entities.") @cache(cacheable: false),
    sessionsListByLocationAndDate (
        locationId: String @doc(description: "Location ID."),
        dateFrom: String! @doc(description: "Date From in format Y-M-D."),
        dateTo: String! @doc(description: "Date To in format Y-M-D."),
        statuses: [String] @doc(description: "Statuses, optional. 0 - Inactive, 1 - Active, 2 - Canceled, 3 - Completed")
    ): [ClassSession]
    @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\SessionsListByLocationAndDate") @doc(description:"Returns a session entities by location and date.") @cache(cacheable: false),
    customSessionsListByLocationAndDate (
        locationId: String! @doc(description: "Location ID."),
        dateFrom: String! @doc(description: "Date From in format Y-M-D."),
        dateTo: String! @doc(description: "Date To in format Y-M-D.")
    ): [CustomSession]
    @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\CustomSessionsListByLocationAndDate") @doc(description:"Returns custom session entities by location and date.") @cache(cacheable: false),
    sessionListByProductId (
        productId: Int! @doc(description: "Product ID."),
        statuses: [String] @doc(description: "Statuses, optional. 0 - Inactive, 1 - Active, 2 - Canceled, 3 - Completed")
    ): [ClassSession]
    @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\SessionsListByProductId") @doc(description:"Returns session entities by Product ID.") @cache(cacheable: false),
    upcomingSessionsByCurrentCustomerId: [ClassSession] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\UpcomingSessions") @doc(description:"Returns upcoming sessions by current Customer ID.") @cache(cacheable: false),
    getWaitListBySessionId (sessionId: Int!): [WaitList] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\WaitLists") @doc(description:"Returns wait list data.") @cache(cacheable: false),
    availableHostsListByDateRange (
        dateStart: String! @doc(description: "Date Start."),
        timeStart: String! @doc(description: "Time Start."),
        dateEnd: String! @doc(description: "Date End."),
        timeEnd: String @doc(description: "Time End.")
    ): [String]
    @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\HostsEmails") @doc(description:"Returns Hosts Emails as array.") @cache(cacheable: false),
    surveyBySessionId (sessionId: Int!): Survey @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\Survey") @doc(description:"Returns survey by session id.") @cache(cacheable: false),
    hashByRegistrationId (registrationId: Int!): String @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\HashByRegistrationId") @doc(description:"Returns hash by registration id.") @cache(cacheable: false),
    upcomingEvents (count: Int): [ClassSession] @resolver (class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\UpcomingEvents") @doc(description:"Returns upcoming Events.") @cache(cacheable: false),
}

type Mutation {
    SendSurvey (input: SendSurveyInput): String @resolver(class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\SendSurvey") @doc(description:"Send Survey"),
    AddToWaitList (input: AddToWaitListInput): String @resolver(class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\AddToWaitList") @doc(description:"Add to Wait List"),
    DeleteFromWaitList (input: DeleteFromWaitList): String @resolver(class: "\\Staylime\\ClassManagerGraphQl\\Model\\Resolver\\DeleteFromWaitList") @doc(description:"Delete from Wait List"),
}

input AddToWaitListInput {
    session_id: Int!
    quantity: Int!
    notes: String
}

input DeleteFromWaitList {
    session_id: Int!
}

input SendSurveyInput {
    registration_id: String!
    hash: String!
    option_rating: String
    options: [OptionInput]!
}

input OptionInput {
    option_id: String
    option_data: String
}

type Registration implements RegistrationInterface @doc(description: "Contains all necessary data about Registration.") {
}

type Survey @doc(description: "Contains wait list data.") {
    id: String @doc(description: "Survey ID.")
    name: String @doc(description: "Survey Name.")
    description: String @doc(description: "Survey Description.")
    use_rating: String @doc(description: "Use rating.")
    options: String @doc(description: "Options.")
    email_content: String @doc(description: "Email content.")
    rating_label: String @doc(description: "Rating label.")
}

type WaitList @doc(description: "Contains wait list data.") {
    entity_id: String @doc(description: "Wait List ID.")
    status: String @doc(description: "Wait List Status.")
    order_id: String @doc(description: "Order ID.")
    order_item_id: String @doc(description: "Order Item ID.")
    customer_id: String @doc(description: "Customer ID.")
    session_id: String @doc(description: "Session ID.")
    name: String @doc(description: "Customer Name.")
    email: String @doc(description: "Customer Email.")
    phone: String @doc(description: "Customer Phone.")
    info: String @doc(description: "Info.")
    note: String @doc(description: "Wait List Note.")
    quantity: String @doc(description: "Quantity.")
    reserved_date: String @doc(description: "Reserved Date.")
    reminders: String @doc(description: "Reminders.")
    meeting_participant_id: String @doc(description: "Meeting Participant Id.")
    meeting_join_link: String @doc(description: "Meeting Join Link.")
    meeting_letter: String @doc(description: "Meeting Letter.")
}

type Location @doc(description: "Contains all necessary data about Location.") {
    location_id: String @doc(description: "Location ID.")
    name: String @doc(description: "Location Name.")
    filter_option_label: String @doc(description: "Filter Option Label.")
    location_code: String @doc(description: "Location Code.")
    location_address: String @doc(description: "Location Address.")
    location_phone: String @doc(description: "Location Phone.")
    is_filterable: String @doc(description: "Is Filterable.")
    is_online: String @doc(description: "Is Online.")
    is_show_calendars: String @doc(description: "Is Show Calendars.")
    location_email: String @doc(description: "Location Name.")
}

type Instructor @doc(description: "Contains all necessary data about Instructor.") {
    instructor_id: String @doc(description: "Instructor ID.")
    name: String @doc(description: "Instructor Name.")
    email: String @doc(description: "Instructor Email.")
    description: String @doc(description: "Description.")
    catalog_url: String @doc(description: "Catalog Url.")
    image: String @doc(description: "Image.")
    zoom_id: String @doc(description: "Zoom ID.")
    zoom_email: String @doc(description: "Zoom Email.")
}

type RegistrationWithDataForCustomer implements RegistrationInterface @doc(description: "Contains all necessary data about Registration.") {
    hours_before_show_link: String @doc(description: "Hours before show link."),
    has_reserved: Boolean @doc(description: "Has reserved."),
    status_code: String @doc(description: "Status code."),
    zoom_meeting_link: [String] @doc(description: "Zoom meeting link."),
    status_name: String @doc(description: "Status name."),
    calendar_file_link: String @doc(description: "Calendar file link.")
}

interface RegistrationInterface @doc(description: "Contains all necessary data about Registration.") @typeResolver(class: "Staylime\\ClassManagerGraphQl\\Model\\RegistrationsResolver") {
    session: ClassSession @doc(description: "Session."),
    order_increment_id: String @doc(description: "Order increment id."),
    entity_id: String @doc(description: "Entity ID."),
    status: String @doc(description: "Status."),
    order_id: String @doc(description: "Order ID."),
    order_item_id: String @doc(description: "Order item ID."),
    customer_id: String @doc(description: "Customer ID."),
    session_id: String @doc(description: "Session ID."),
    name: String @doc(description: "Name."),
    email: String @doc(description: "Email."),
    phone: String @doc(description: "Phone."),
    info: String @doc(description: "Info."),
    note: String @doc(description: "Note."),
    reserved_date: String @doc(description: "Reserved date."),
    reminders: String @doc(description: "Reminders."),
    meeting_participant_id: String @doc(description: "Meeting participant ID."),
    meeting_join_link: String @doc(description: "Meeting join link."),
    meeting_letter: String @doc(description: "Meeting letter.")
}

type ClassSession implements ClassSessionInterface @doc(description: "Contains all necessary data about Session.") {
    product: ProductInterface @doc(description: "Product data.")
}

type CustomSession implements ClassSessionInterface @doc(description: "Contains all necessary data about Custom Session.") {
}

interface ClassSessionInterface @doc(description: "Contains all necessary data about Session.") @typeResolver(class: "Staylime\\ClassManagerGraphQl\\Model\\SessionsResolver") {
    location_name: String @doc(description: "Location name."),
    date_time: String @doc(description: "Date Time."),
    instructor_name: String @doc(description: "Instructor name."),
    entity_id: String @doc(description: "Entity ID."),
    product_id: String @doc(description: "Product ID."),
    sku: String @doc(description: "SKU."),
    status: String @doc(description: "Status."),
    date_start: String @doc(description: "Date start."),
    date_end: String @doc(description: "Date end."),
    seats_count: String @doc(description: "Seats count."),
    price_adjustment: String @doc(description: "Price adjustment."),
    is_special: String @doc(description: "Is special."),
    note: String @doc(description: "Note."),
    no_session_message: String @doc(description: "No session message."),
    creation_date: String @doc(description: "Creation date."),
    meeting_host: String @doc(description: "Meeting host."),
    meeting_link: String @doc(description: "Meeting link."),
    meeting_password: String @doc(description: "Meeting password."),
    meeting_id: String @doc(description: "Meeting ID."),
    meeting_credentials_notification: String @doc(description: "Meeting credentials notification."),
    survey_id: String @doc(description: "Survey ID."),
    location: String @doc(description: "Location"),
    instructor: String @doc(description: "Instructor.")
}

type StoreConfig {
    tl_class_manager_product_mode_without_active_sessions: String @doc(description: "Stock Status for Classes with No Active Sessions."),
    tl_class_manager_product_show_instructors_tab: String @doc(description: "Show Instructors Tab."),
    tl_class_manager_registration_store_groups: String @doc(description: "Customer Groups Enabled for In-Store Registration."),
    tl_class_manager_registration_wait_list_enabled: String @doc(description: "Wait Lists."),
    tl_class_manager_registration_wl_show_session_options: String @doc(description: "Include Product Options in Wait List Form."),
    tl_class_manager_registration_auto_open_reservation_form: String @doc(description: "Auto-Open Registration Form."),
    tl_class_manager_registration_supply_list_note: String @doc(description: "Text for Supply List Items (Related Products)."),
    tl_class_manager_session_available_seats_notify: String @doc(description: "Quantity at Which to Show Number of Seats Available."),
    tl_class_manager_session_showed_count: String @doc(description: "Number of Sessions to Highlight on Product Page."),
    tl_class_manager_session_cart_counting_disabled: String @doc(description: "Disallow Session Quantity Adjustments in Cart."),
    tl_class_manager_session_no_session_text: String @doc(description: "Default Text for No Sessions."),
    tl_class_manager_survey_default_title: String @doc(description: "Default Rating Option Label."),
    tl_class_manager_other_top_menu_calendar: String @doc(description: "Add Events Calendar to Top Menu."),
    tl_class_manager_other_add_to_calendar_enabled: String @doc(description: "Add to Calendar Links."),
    tl_class_manager_other_show_busy_instructors: String @doc(description: "Include Instructors with No Sessions Available on Instructors Page.")
}
